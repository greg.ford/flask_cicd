from flask import *
import os

app = Flask(__name__)


@app.route("/")
def handle_root():
    return """<!DOCTYPE html>
        <html>
            <head>
                <title>
                    Test
                </title>
            </head>
            <body>
                <h1>HELLO WORLD</h1>
            </body>
        </html>
    
    """

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=os.environ["PORT"])
